import buble from 'rollup-plugin-buble';

export default {
	input: 'src/version.mjs',
	plugins: [ buble() ],
	external: [ '@rollup/plugin-json', 'fs', 'path' ]
};
