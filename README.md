# rollup-plugin-git-version

[Rollup](http://www.rollupjs.org) plugin to include the git rev in the version of `package.json`.


If you develop some software which is bundled with RollupJS, how many times have
you done something like...?

```
import { version } from '../package.json';
```

And how many times have you wished that the current git branch and revision would
be included in that version string?

If the answer is "more than zero", this RollupJS plugin is for you.

So if your package.json contains something like...

```
{
	version: "major.minor.patch"
	...
```

Then, by using this plugin, when doing an `import { version } from '../package.json';`,
`version` will have a value like `"major.minor.patch+branchname.revision"`. This
is in compliance with the [semantic versioning standard](http://semver.org/) about
build metadata.

For example, if your `package.json` contains `version: "1.2.3"`, then the value
for the `version` value inside your rolled-up code will not be `"1.2.3"`, but
instead `"1.2.3+master.890abc"`.


## Installation

```bash
yarnpkg add --dev rollup-plugin-git-version
```
or
```bash
npm install --save-dev rollup-plugin-git-version
```


## Usage

As with any other Rollup plugin, just add it to the `plugins` option in your RollupJS config:

```js
// In rollup.config.js
import rollupGitVersion from 'rollup-plugin-git-version'

export default {
	entry: 'src/index.js',
	dest: 'dist/my-lib.js',
	plugins: [
		rollupGitVersion()
	]
};
```

This plugin accepts the same options as [`@rollup/plugin-json`](https://github.com/rollup/plugins/tree/master/packages/json).

If you use this plugin together with [`@rollup/plugin-json`](https://github.com/rollup/plugins/tree/master/packages/json),
please be aware that both plugins might try to fight for the file. You can use the `exclude` option of `@rollup/plugin-json`, e.g.:

```js
// In rollup.config.js
import rollupGitVersion from 'rollup-plugin-git-version'

export default {
	entry: 'src/index.js',
	dest: 'dist/my-lib.js',
	plugins: [
		rollupGitVersion(),
		json({ exclude: 'package.json' })
	]
};
```



## License

"THE BEER-WARE LICENSE":
<ivan@sanchezortega.es> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return.
